# VirtLCD
VirtLCD是一个通用的LCD模拟器，可以利用它在Windows系统的电脑上模拟嵌入式GUI开发。

# 工作原理
**virtlcd.exe** 是服务器程序，负责创建窗口，创建显存，并把显存的数据显示到屏幕上。

**virtlcd.dll** 是客户端动态链接库，用户程序调用它的接口，可以获取到显存地址，直接往该地址写数据就能显示到LCD屏幕上，同时它和服务器通过UDP通信，接收按键和鼠标消息。

> 使用独立进程来绘制屏幕的好处就是方便调试。用户程序在画点时打断点，服务器进程不受影响，可以实时输出显示内容。

# API接口

```
//启动VirtLCD，设置分辨率，色深，放大倍数
BOOL VirtLCD_Init(int width, int height, int bpp, int scale);

//关闭VirtLCD
void VirtLCD_Exit(void);

//设置按键消息回调函数
void VirtLCD_SetKeybdProc(KEYBDPROC func);

//设置鼠标消息回调函数
void VirtLCD_SetMouseProc(MOUSEPROC func);

//获取LCD帧缓存
void *VirtLCD_GetFrameBuffer(void);
```

> 注意：API使用的调用约定是__cdecl，这个也是编译器的默认值。

# 示例程序

参考`example`目录下的代码，参考使用方法。
