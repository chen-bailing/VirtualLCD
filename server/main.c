/************************************************************************
* 虚拟LCD服务器进程,创建内存映射和UDP服务器
* 内存映射用作显示BUFFER(w*h*bpp)
* UDP服务器用作发送输入消息
* 命令行参数: width=%d height=%d bpp=16/24/32 scale=1/2/3/4
* 蒋晓岗<kerndev@foxmail.com>
************************************************************************/
#include "stdafx.h"
#include "server.h"
#include "resource.h"

#define	CLASS_NAME    _T("VirtLCD")
#define MAPPING_NAME  _T("VirtLCD/Mapping")
#define SERVER_PORT   8051

static UINT      m_nLCDScale;
static UINT      m_nLCDWidth;
static UINT      m_nLCDHeight;
static UINT      m_nLCDBitsPerPixel;
static HINSTANCE m_hGlobalInst;
static HANDLE    m_hGlobalMapping;
static HANDLE    m_hGlobalEvent;
static HWND      m_hMainWnd;
static UINT      m_nWndWidth;
static UINT      m_nWndHeight;
static HDC       m_hBufferDC;
static HBITMAP   m_hBufferBitmap;
static DWORD    *m_pBufferBits;
static DWORD    *m_pBufferMapping;
static DWORD     m_dwBufferLen;
static BOOL      m_bHasClient;

static void OnRecvRequest(BYTE cmd, BYTE *data, INT len)
{
	switch(cmd)
	{
	case 0x00: //连接
		m_bHasClient = TRUE;
		server_send_event(0x00, NULL, 0);
		break;
	case 0x01: //退出
		m_bHasClient = FALSE;
		PostMessage(m_hMainWnd, WM_CLOSE, 0, 0);
		break;
	default:
		break;
	}
}

//解析命令行参数
static void ParsmCommandLine(void)
{
	int i;
	int argc;
	LPWSTR cmd;
	LPWSTR *argv;
	m_nLCDScale  = 1;
	m_nLCDWidth  = 400;
	m_nLCDHeight = 300;
	m_nLCDBitsPerPixel = 16;
	cmd = GetCommandLineW();
	argv= CommandLineToArgvW(cmd, &argc);
	for(i=0; i<argc; i++)
	{
		if(wcsncmp(argv[i], L"width=", 6)==0)
		{
			m_nLCDWidth = _wtoi(argv[i]+6);
			continue;
		}
		if(wcsncmp(argv[i], L"height=", 7)==0)
		{
			m_nLCDHeight = _wtoi(argv[i] + 7);
			continue;
		}
		if(wcsncmp(argv[i], L"bpp=", 4)==0)
		{
			m_nLCDBitsPerPixel = _wtoi(argv[i] + 4);
			continue;
		}
		if(wcsncmp(argv[i], L"scale=", 6)==0)
		{
			m_nLCDScale = _wtoi(argv[i] + 6);
			continue;
		}
	}
	m_dwBufferLen = m_nLCDWidth * m_nLCDHeight * (m_nLCDBitsPerPixel / 8);
}

//创建EVENT，检测实例
static BOOL CreateGlobalEvent(void)
{
	m_hGlobalEvent = CreateEvent(NULL, 0, 0, CLASS_NAME);
	if(GetLastError()==ERROR_ALREADY_EXISTS)
	{
		return FALSE;
	}
	return TRUE;
}

static void DeleteGlobalEvent(void)
{
	CloseHandle(m_hGlobalEvent);
}

//初始化内存映射
static BOOL CreateMapping(void)
{
	m_hGlobalMapping = CreateFileMapping(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,m_dwBufferLen,MAPPING_NAME);
	if(m_hGlobalMapping == NULL)
	{
		return FALSE;
	}
	m_pBufferMapping = (DWORD*)MapViewOfFile(m_hGlobalMapping,FILE_MAP_ALL_ACCESS,0,0,m_dwBufferLen);
	if(m_pBufferMapping == NULL)
	{
		CloseHandle(m_hGlobalMapping);
		return FALSE;
	}
	return TRUE;
}

//释放内存映射
static void DeleteMapping(void)
{
	UnmapViewOfFile(m_pBufferMapping);
	CloseHandle(m_hGlobalMapping);
}

//创建内存DC和BITMAP
static BOOL CreateBuffer(HWND hWnd)
{
	HDC hdc;
	BITMAPINFO *bmi;
	UINT bmi_size;
	bmi_size = sizeof(BITMAPINFO) + sizeof(RGBQUAD) * 4;
	bmi = malloc(bmi_size);
	ZeroMemory(bmi, bmi_size);
	bmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi->bmiHeader.biWidth  = m_nLCDWidth;
	bmi->bmiHeader.biHeight = m_nLCDHeight;
	bmi->bmiHeader.biPlanes = 1;
	bmi->bmiHeader.biBitCount = m_nLCDBitsPerPixel;
	bmi->bmiHeader.biSizeImage = m_dwBufferLen;
	bmi->bmiHeader.biCompression = BI_RGB;
	if(m_nLCDBitsPerPixel == 16)
	{
		//16位色默认是RGB555，需要手动指定为RGB565
		bmi->bmiHeader.biCompression = BI_BITFIELDS;
		bmi->bmiColors[0].rgbBlue = 0;
		bmi->bmiColors[0].rgbGreen = 0xF8;
		bmi->bmiColors[0].rgbRed = 0;
		bmi->bmiColors[0].rgbReserved = 0;
		bmi->bmiColors[1].rgbBlue = 0xE0;
		bmi->bmiColors[1].rgbGreen = 0x07;
		bmi->bmiColors[1].rgbRed = 0;
		bmi->bmiColors[1].rgbReserved = 0;
		bmi->bmiColors[2].rgbBlue = 0x1F;
		bmi->bmiColors[2].rgbGreen = 0;
		bmi->bmiColors[2].rgbRed = 0;
		bmi->bmiColors[2].rgbReserved = 0;
	}
	hdc = GetDC(hWnd);
	m_hBufferDC = CreateCompatibleDC(hdc);
	if(m_hBufferDC == NULL)
	{
		ReleaseDC(hWnd,hdc);
		return FALSE;
	}
	m_hBufferBitmap = CreateDIBSection(hdc,bmi,DIB_RGB_COLORS,(void**)&m_pBufferBits,NULL,0);
	if(m_hBufferBitmap == NULL)
	{
		DeleteDC(m_hBufferDC);
		ReleaseDC(hWnd,hdc);
		return FALSE;
	}
	SelectObject(m_hBufferDC, m_hBufferBitmap);
	ReleaseDC(hWnd,hdc);
	return TRUE;
}

//释放内存DC和BITMAP
static void DeleteBuffer(void)
{
	DeleteDC(m_hBufferDC);
	DeleteObject(m_hBufferBitmap);
}

static void OnKeybd(UINT msg, WPARAM wParam)
{
	BYTE arg[4];
	if(m_bHasClient)
	{
		arg[0] = msg!=WM_KEYDOWN;
		arg[1] = (BYTE)wParam;
		server_send_event(0x20, arg, 2);
	}
}

void OnMouse(UINT msg, int x, int y)
{
	BYTE arg[8];
	RECT rc;
	x = x / m_nLCDScale;
	y = y / m_nLCDScale;
	if(m_bHasClient)
	{
		arg[0] = 0;
		arg[1] = (BYTE)(x>>8);
		arg[2] = (BYTE)(x&0xFF);
		arg[3] = (BYTE)(y>>8);
		arg[4] = (BYTE)(y&0xFF);
		switch(msg)
		{
		case WM_LBUTTONDOWN:
			arg[0] = 0;
			server_send_event(0x21, arg, 5);
			GetClientRect(m_hMainWnd, &rc);
			MapWindowPoints(m_hMainWnd, NULL, (POINT*)&rc, 2);
			ClipCursor(&rc);
			break;
		case WM_LBUTTONUP:
			arg[0] = 1;
			server_send_event(0x21, arg, 5);
			ClipCursor(NULL);
			break;
		case WM_MOUSEMOVE:
			arg[0] = 2;
			server_send_event(0x21, arg, 5);
			break;
		default:
			break;
		}
	}
}

LRESULT OnCreate(HWND hWnd, LPCREATESTRUCT cs)
{
	char text[256];
	if(!CreateMapping())
	{
		sprintf_s(text,256,"创建内存映射失败(E=%d)!",GetLastError());
		MessageBox(NULL,text,"VirtLCD",MB_OK|MB_ICONERROR);
		return -1;
	}
	if(!CreateBuffer(hWnd))
	{
		sprintf_s(text,256,"创建GDI对象失败(E=%d)!",GetLastError());
		MessageBox(NULL,text,"VirtLCD",MB_OK|MB_ICONERROR);
		DeleteMapping();
		return -1;
	}
	server_init();
	if(!server_start(SERVER_PORT, OnRecvRequest))
	{
		sprintf_s(text,256,"创建UDP服务失败(E=%d)!",GetLastError());
		MessageBox(NULL,text,"VirtLCD",MB_OK|MB_ICONERROR);
		DeleteBuffer();
		DeleteMapping();
		return -1;
	}
	//设置刷新频率
	SetTimer(hWnd, 1, 20, NULL);
	return 0;
}

void OnClose(HWND hWnd)
{
	DeleteMapping();
	DeleteBuffer();
	DestroyWindow(hWnd);
}

void OnErase(HWND hWnd, HDC hdc)
{
	UINT i;
	BYTE *dst;
	BYTE *src;
	UINT line_size;
	dst = (BYTE *)m_pBufferBits;
	src = (BYTE *)m_pBufferMapping;
	src = src + m_dwBufferLen;
	line_size = m_nLCDWidth * (m_nLCDBitsPerPixel / 8);
	for(i=0; i<m_nLCDHeight; i++)
	{
		src -= line_size;
		CopyMemory(dst, src, line_size);
		dst += line_size;
	}
	StretchBlt(hdc, 0, 0, m_nLCDScale * m_nLCDWidth, m_nLCDScale * m_nLCDHeight, m_hBufferDC, 0, 0, m_nLCDWidth, m_nLCDHeight, SRCCOPY);
}

void OnSysCommand(HWND hWnd, WPARAM wParam)
{
	if(wParam == SC_CLOSE)
	{
		OnKeybd(WM_KEYDOWN, VK_SLEEP);
	}
}

static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		return OnCreate(hWnd,(LPCREATESTRUCT)lParam);
	case WM_CLOSE:
		OnClose(hWnd);
		return 0;
	case WM_SYSCOMMAND:
		OnSysCommand(hWnd, wParam);
		break;
	case WM_KEYUP:
	case WM_KEYDOWN:
		OnKeybd(message,wParam);
		return 0;
	case WM_MOUSEMOVE:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
		OnMouse(message,GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam));
		return 0;
	case WM_ERASEBKGND:
		OnErase(hWnd,(HDC)wParam);
		return TRUE;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	case WM_TIMER:
		InvalidateRect(hWnd,NULL,TRUE);
		return 0;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

static ATOM RegisterWindow(void)
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style          = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc    = WndProc;
	wcex.cbClsExtra     = 0;
	wcex.cbWndExtra     = 0;
	wcex.hInstance      = m_hGlobalInst;
	wcex.hIcon          = LoadIcon(m_hGlobalInst, MAKEINTRESOURCE(IDI_ICON1));
	wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground  = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName   = NULL;
	wcex.lpszClassName  = CLASS_NAME;
	wcex.hIconSm        = NULL;
	return RegisterClassEx(&wcex);
}

static BOOL InitWindow(void)
{
	int x;
	int y;
	int w;
	int h;
	RECT  rc;
	DWORD style;
	TCHAR name[256];
	style = WS_DLGFRAME|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX;
	SetRect(&rc, 0, 0, m_nLCDScale * m_nLCDWidth, m_nLCDScale * m_nLCDHeight);
	AdjustWindowRect(&rc, style, FALSE);
	w = rc.right - rc.left;
	h = rc.bottom- rc.top;
	x = (GetSystemMetrics(SM_CXSCREEN)-w)/2;
	y = (GetSystemMetrics(SM_CYSCREEN)-h)/2;
	sprintf_s(name, sizeof(name), "VirtLCD %dx%dx%dbpp X%d", m_nLCDWidth, m_nLCDHeight, m_nLCDBitsPerPixel, m_nLCDScale);
	m_hMainWnd = CreateWindowEx(0,CLASS_NAME, name, style, x, y, w, h, NULL, NULL, m_hGlobalInst, NULL);
	if(m_hMainWnd != NULL)
	{
		ShowWindow(m_hMainWnd,SW_SHOW);
		UpdateWindow(m_hMainWnd);
		return TRUE;
	}
	return FALSE;
}

int PumpMessage(void)
{
	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0))
	{
		DispatchMessage(&msg);
	}
	return (int)msg.wParam;
}

int APIENTRY WinMain(HINSTANCE hInst,HINSTANCE hPrev,LPTSTR lpCmdLine,int nCmdShow)
{
	WSADATA wsa;
	m_hGlobalInst = hInst;
	m_bHasClient = FALSE;
	if(!CreateGlobalEvent())
	{
		MessageBox(NULL,"只能存在一个实例!","VirtLCD",MB_OK|MB_ICONWARNING);
		return -1;
	}
	ParsmCommandLine();
	if(WSAStartup(MAKEWORD(2,2), &wsa)!=0)
	{
		MessageBox(NULL,"SOCKET初始化失败!","VirtLCD",MB_OK|MB_ICONERROR);
		DeleteGlobalEvent();
		return -1;
	}
	RegisterWindow();
	if(!InitWindow())
	{
		MessageBox(NULL,"创建主窗口失败!","VirtLCD",MB_OK|MB_ICONERROR);
		DeleteGlobalEvent();
		return -1;
	}
	PumpMessage();
	WSACleanup();
	DeleteGlobalEvent();
	return 0;
}
