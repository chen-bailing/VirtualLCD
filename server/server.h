#pragma once

typedef void(*recv_request_t)(BYTE cmd, BYTE *data, INT len);

void server_init(void);
BOOL server_start(int port, recv_request_t func);
void server_stop(void);
BOOL server_send_event(BYTE cmd, BYTE* arg, BYTE len);
