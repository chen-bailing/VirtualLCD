/******************************************************************************
* UDP协议与客户程序通信
* MAGIC,CMD,DATA...
* MAGIC=0xAA
* CMD =命令字节
* DATA=数据字节
*******************************************************************************
* CMD   发送方	定义			数据字节
* 0x00  客户端	连接握手       0x00
* 0x00  服务器 连接确认       0x00
* 0x01  客户端 关闭连接       0x00
* 0x20  服务器	发送键盘事件	EVENT|VK
* 0x21	服务器	发送鼠标事件	EVENT|XH|XL|YH|YL
*******************************************************************************
* 简写定义:
* VK:   按键码
* 键盘: EVENT=0,按键按下;EVENT=1,按键弹起
* 鼠标: EVENT=0,左键按下;EVENT=1,左键弹起;EVENT=2,鼠标移动
*****************************************************************************/
#include "stdafx.h"
#include "server.h"

static HANDLE m_thread;
static SOCKET m_socket;
static SOCKADDR_IN m_addr;
static recv_request_t on_recv_request;

static DWORD WINAPI recv_thread_entry(LPVOID arg)
{
	int recv_len;
	int addr_len;
	BYTE recv_buf[64];
	struct sockaddr_in addr;
	while(1)
	{
		addr_len = sizeof(addr);
		recv_len = recvfrom(m_socket,(char*)recv_buf,sizeof(recv_buf),0,(struct sockaddr*)&addr,&addr_len);
		if(recv_len > 1)
		{
			if(recv_buf[0] == 0xAA)
			{
				memcpy(&m_addr,&addr,sizeof(m_addr));
				on_recv_request(recv_buf[1],&recv_buf[2],recv_len-2);
			}
		}
	}
}

void server_init(void)
{
	m_socket = 0;
	m_thread = NULL;
	on_recv_request = NULL;
	memset(&m_addr, 0, sizeof(m_addr));
}

BOOL server_start(int port, recv_request_t func)
{
	DWORD tid;
	struct sockaddr_in addr;
	if(m_thread != NULL)
	{
		return FALSE;
	}
	m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(m_socket == INVALID_SOCKET)
	{
		return FALSE;
	}
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.S_un.S_addr = INADDR_ANY;
	addr.sin_port = htons(port);
	if(bind(m_socket,(struct sockaddr *)&addr,sizeof(addr))==SOCKET_ERROR)
	{
		closesocket(m_socket);
		return FALSE;
	}
	m_thread = CreateThread(NULL,0,recv_thread_entry,NULL,0,&tid);
	if(m_thread == NULL)
	{
		closesocket(m_socket);
		return FALSE;
	}
	on_recv_request = func;
	return TRUE;
}

void server_stop(void)
{
	if(m_socket!=0)
	{
		closesocket(m_socket);
		m_socket = 0;
	}
	if(m_thread != NULL)
	{
		TerminateThread(m_thread,0);
		CloseHandle(m_thread);
		m_thread = NULL;
	}
}

BOOL server_send_event(BYTE cmd, BYTE* arg, BYTE len)
{
	int  ret;
	BYTE data[64];
	if(m_socket)
	{
		data[0] = 0xAA;
		data[1] = cmd;
		memcpy(&data[2], arg, len);
		ret = sendto(m_socket,(const char*)data,2+len,0,(struct sockaddr*)&m_addr,sizeof(m_addr));
		return ret!=SOCKET_ERROR;
	}
	return FALSE;
}
