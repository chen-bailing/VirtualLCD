/******************************************************************************
* @file   client
* @brief  客户端程序
*         1.连接服务器
*         2.获取按键事件
*         3.获取鼠标事件
*         4.获取显示内存
* @author 蒋晓岗<kerndev@foxmail.com>
* @date   2022.03.23
*****************************************************************************/
#include "stdafx.h"
#include "client.h"
#include "virtlcd.h"

#define SERVER_NAME		_T("VirtLCD.exe")
#define SERVER_PORT		8051
#define MAPPING_NAME	_T("VirtLCD/Mapping")

static HANDLE    m_event;
static HANDLE    m_mapping;
static DWORD    *m_mapping_mem;
static KEYBDPROC m_keybd_handler;
static MOUSEPROC m_mouse_handler;
static char      m_module_path[1024];

static void on_connected(BYTE *data, INT len)
{
	SetEvent(m_event);
}

static void on_keybd_event(BYTE *data, INT len)
{
	if((len > 1) && (m_keybd_handler != NULL))
	{
		m_keybd_handler(data[0],data[1]);
	}
}

static void on_mouse_event(BYTE *data, INT len)
{
	int x;
	int y;
	if((len > 4) && (m_mouse_handler != NULL))
	{
		x = (data[1]<<8)|data[2];
		y = (data[3]<<8)|data[4];
		m_mouse_handler(data[0],x,y);
	}
}

static void dispatch_event(BYTE cmd, BYTE *data, INT len)
{
	switch(cmd)
	{
	case 0x00: //连接应答
		on_connected(data,len);
		break;
	case 0x20: //键盘消息
		on_keybd_event(data,len);
		break;
	case 0x21: //鼠标消息
		on_mouse_event(data,len);
		break;
	default:
		break;
	}
}

static BOOL create_server(int width, int height, int bpp, int scale)
{
	int i;
	char cmdline[256];
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	sprintf_s(cmdline, sizeof(cmdline), "%s width=%d height=%d bpp=%d scale=%d", SERVER_NAME, width, height, bpp, scale);
	GetStartupInfo(&si);
	if(!CreateProcess(NULL,cmdline,NULL,NULL,FALSE,0,NULL,m_module_path,&si,&pi))
	{
		MessageBox(NULL, "启动服务器进程失败!", "VirtLCD", MB_ICONERROR);
		return FALSE;
	}
	client_init();
	if(!client_start(SERVER_PORT, dispatch_event))
	{
		MessageBox(NULL, "启动客户端服务失败!", "VirtLCD", MB_ICONERROR);
		TerminateProcess(pi.hProcess, -1);
		CloseHandle(pi.hProcess);
		return FALSE;
	}
	for(i=0; i<10; i++)
	{
		client_send_request(0x00, NULL, 0);
		if(WaitForSingleObject(m_event, 100) == WAIT_OBJECT_0)
		{
			return TRUE;
		}
	}
	MessageBox(NULL, "连接服务器失败!", "VirtLCD", MB_ICONERROR);
	TerminateProcess(pi.hProcess, -1);
	CloseHandle(pi.hProcess);
	return FALSE;
}

static void close_server(void)
{
	client_send_request(0x01, NULL, 0);
}

static BOOL open_mapping(void)
{
	m_mapping = OpenFileMapping(FILE_MAP_ALL_ACCESS, 0, MAPPING_NAME);
	if(m_mapping == NULL)
	{
		MessageBox(NULL, "打开内存映射失败!", "VirtLCD", MB_ICONERROR);
		return FALSE;
	}
	m_mapping_mem = (DWORD*)MapViewOfFile(m_mapping,FILE_MAP_ALL_ACCESS,0,0,0);
	if(m_mapping_mem == NULL)
	{
		MessageBox(NULL, "获取内存映射失败!", "VirtLCD", MB_ICONERROR);
		CloseHandle(m_mapping);
		return FALSE;
	}
	return TRUE;
}

static void close_mapping(void)
{
	if(m_mapping_mem != NULL)
	{
		UnmapViewOfFile(m_mapping_mem);
		CloseHandle(m_mapping);
		m_mapping = NULL;
		m_mapping_mem = NULL;
	}
}

static BOOL get_module_path(HMODULE module)
{
	char *find;
	memset(m_module_path,0,sizeof(m_module_path));
	GetModuleFileName(module,m_module_path,sizeof(m_module_path));
	find = strchr(m_module_path,'\\');
	if(find != NULL)
	{
		*find = '\0';
		return TRUE;
	}
	return FALSE;
}

static BOOL module_init(HMODULE module)
{
	int ret;
	WSADATA wsa;
	m_keybd_handler = NULL;
	m_mouse_handler = NULL;
	m_mapping = NULL;
	m_mapping_mem = NULL;
	m_event = CreateEvent(NULL,FALSE,FALSE,NULL);
	get_module_path(module);
	ret = WSAStartup(MAKEWORD(2,2), &wsa);
	return (ret==0);
}

static void module_exit(void)
{
	close_server();
	close_mapping();
	CloseHandle(m_event);
	WSACleanup();
}

/*****************************************************************************
* API
*****************************************************************************/
int VirtLCD_Init(int width, int height, int bpp, int scale)
{
	if(!create_server(width, height, bpp, scale))
	{
		return 0;
	}
	if(!open_mapping())
	{
		close_server();
		return 0;
	}
	return 1;
}

void VirtLCD_Exit(void)
{
	close_server();
	close_mapping();
}

void *VirtLCD_GetFrameBuffer(void)
{
	return m_mapping_mem;
}

void VirtLCD_SetKeybdProc(KEYBDPROC func)
{
	m_keybd_handler = func;
}

void VirtLCD_SetMouseProc(MOUSEPROC func)
{
	m_mouse_handler = func;
}

BOOL APIENTRY DllMain(HMODULE hModule,DWORD dwReason, LPVOID lpReserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		return module_init(hModule);
	case DLL_PROCESS_DETACH:
		module_exit();
		break;
	default:
		break;
	}
	return TRUE;
}
