#pragma once

typedef void(*recv_process_t)(BYTE cmd, BYTE *data, INT len);

void client_init(void);
BOOL client_start(int port, recv_process_t proc);
void client_stop(void);
BOOL client_send_request(BYTE cmd, BYTE *arg, BYTE len);
