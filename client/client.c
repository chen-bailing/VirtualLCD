/******************************************************************************
* @file   client
* @brief  UDP协议客户端
*         协议：0xAA+CMD+DATA
* @author 蒋晓岗<kerndev@foxmail.com>
*****************************************************************************/
#include "stdafx.h"
#include "client.h"

static HANDLE m_thread;
static SOCKET m_socket;
static SOCKADDR_IN m_addr;
static recv_process_t on_recv_resp;

void client_init(void)
{
	m_socket = 0;
	m_thread = NULL;
	on_recv_resp = NULL;
	memset(&m_addr, 0, sizeof(m_addr));
}

static DWORD WINAPI recv_thread_entry(LPVOID arg)
{
	int recv_len;
	int addr_len;
	BYTE recv_buf[64];
	struct sockaddr_in addr;
	while(1)
	{
		addr_len = sizeof(addr);
		recv_len = recvfrom(m_socket, (char *)recv_buf, sizeof(recv_buf), 0, (struct sockaddr *)&addr, &addr_len);
		if(recv_len > 1)
		{
			if(recv_buf[0] == 0xAA)
			{
				on_recv_resp(recv_buf[1], &recv_buf[2], recv_len-2);
			}
		}
	}
}

BOOL client_start(int port, recv_process_t proc)
{
	DWORD tid;
	if(m_thread != NULL)
	{
		return FALSE;
	}
	m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(m_socket == INVALID_SOCKET)
	{
		return FALSE;
	}
	memset(&m_addr, 0, sizeof(m_addr));
	m_addr.sin_family = AF_INET;
	m_addr.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	m_addr.sin_port = htons(port);
	m_thread = CreateThread(NULL, 0, recv_thread_entry, NULL, 0, &tid);
	if(m_thread == NULL)
	{
		closesocket(m_socket);
		return FALSE;
	}
	on_recv_resp = proc;
	return TRUE;
}

void client_stop(void)
{
	if(m_socket != 0)
	{
		closesocket(m_socket);
		m_socket = 0;
	}
	if(m_thread != NULL)
	{
		TerminateThread(m_thread, 0);
		CloseHandle(m_thread);
		m_thread = NULL;
	}
}

BOOL client_send_request(BYTE cmd, BYTE *arg, BYTE len)
{
	int ret;
	BYTE data[32];
	if(m_socket != 0)
	{
		data[0] = 0xAA;
		data[1] = cmd;
		memcpy(&data[2], arg, len);
		ret = sendto(m_socket,(const char*)data, len + 2, 0, (struct sockaddr*)&m_addr, sizeof(m_addr));
		return ret != SOCKET_ERROR;
	}
	return 0;
}
