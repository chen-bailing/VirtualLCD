/******************************************************************************
* @file   main
* @brief  演示VirtLCD的使用方法
*         引用头文件:virtlcd.h
*         引用动态库:virtlcd.lib
* @author 蒋晓岗<kerndev@foxmail.com>
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "windows.h"
#include "virtlcd.h"

#pragma comment(lib,"virtlcd.lib")

#define LCD_X_SIZE 480 //水平分辨率
#define LCD_Y_SIZE 320 //垂直分辨率
#define LCD_BPP    16  //颜色位深: RGB565
#define LCD_SCALE  2   //放大2倍，像素点看得更清楚

static UINT16 *m_fb;
static BOOL    m_clicked;
static BOOL    m_exit;
static UINT16  m_color = 0xFFFF;

//画一个点
void lcd_draw_point(int x, int y, UINT16 color)
{
	m_fb[y * LCD_X_SIZE + x] = color;
}

//处理按键事件
void on_keybd_event(int event, int key)
{
	if(event == KEYBD_EVENT_DOWN)
	{
		printf("按键按下: %d\n", key);
		switch(key)
		{
		case '1':
			m_color = 0xFFFF;
			printf("白色\n");
			break;
		case '2':
			m_color = 0xF800;
			printf("红色\n");
			break;
		case '3':
			m_color = 0x07E0;
			printf("绿色\n");
			break;
		case '4':
			m_color = 0x001F;
			printf("蓝色\n");
			break;
		case '0':
			m_color = 0x0000;
			printf("黑色\n");
			break;
		case 27:
			m_exit = TRUE;
			printf("程序退出\n");
			break;
		}
	}
	if(event == KEYBD_EVENT_UP)
	{
		printf("按键弹起: %d\n", key);
	}
}

//处理鼠标事件
void on_mouse_event(int event, int x, int y)
{
	if(event == MOUSE_EVENT_LBDOWN)
	{
		printf("鼠标按下: X=%d, Y=%d\n", x, y);
		m_clicked = 1;
	}
	if(event == MOUSE_EVENT_LBUP)
	{
		printf("鼠标弹起: X=%d, Y=%d\n", x, y);
		m_clicked = 0;
	}
	if(event == MOUSE_EVENT_MOVE)
	{
		printf("鼠标移动: X=%d, Y=%d\n", x, y);
		if(m_clicked)
		{
			lcd_draw_point(x, y, m_color);
		}
	}
}

void main(void)
{
	int ret;
	ret = VirtLCD_Init(LCD_X_SIZE, LCD_Y_SIZE, LCD_BPP, LCD_SCALE);
	if(ret == 0)
	{
		printf("初始化VirtLCD失败!\n");
		system("pause");
		return;
	}
	VirtLCD_SetKeybdProc(on_keybd_event);
	VirtLCD_SetMouseProc(on_mouse_event);
	m_fb = (UINT16 *)VirtLCD_GetFrameBuffer();
	printf("获取帧缓存: FrameBuffer = 0x%p\n", m_fb);
	printf("初始化成功!\n");
	while(m_exit==0)
	{
		Sleep(100);
	}
	VirtLCD_Exit();
}
